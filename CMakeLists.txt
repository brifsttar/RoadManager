message(STATUS "CMake version: " ${CMAKE_VERSION})

# Set to the current lowest tested version of CMake
if(WIN32)
  # for cmake generator VisualStudio 2017 support
  cmake_minimum_required (VERSION 3.7.0 FATAL_ERROR)
else()
  cmake_minimum_required (VERSION 2.8.12 FATAL_ERROR)
endif()

project (EnvironmentSimulator)

if(MSVC)
  set(CMAKE_VS_INCLUDE_INSTALL_TO_DEFAULT_BUILD 1)
elseif (MINGW)
endif()

set(CMAKE_CXX_STANDARD 14)

# Allow for folder structure
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# The static libaries needs to be compiled with position independent code
# otherwise we cant link with them when building dynamic libaries
# An alternative to this global option is to set POSITION_INDEPENDENT_CODE on each target
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

if (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
  set (LINUX true)
else ()
  set (LINUX false)
endif ()

if (LINUX OR APPLE OR MINGW OR MSVC)
  set(INSTALL_DIRECTORY "${CMAKE_HOME_DIRECTORY}/bin")
else ()
  message(FATAL_ERROR "Unrecognized platform therefore there isn't an installation directory. Stopping the cmake process.")
endif ()

set(INSTALL_DIRECTORY_CODE_EXAMPLES "${CMAKE_HOME_DIRECTORY}/code-examples-bin")

set(PUGIXML_INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/externals/pugixml")
set(EXPR_INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/externals/expr")
if(MSVC)
    set(DIRENT_INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/externals/dirent/win")
else()
    set(DIRENT_INCLUDE_DIR "")
endif()


include_directories( ${PUGIXML_INCLUDE_DIR} ${EXPR_INCLUDE_DIR})

add_subdirectory(EnvironmentSimulator)

# Add variables to global scope, e.g. when esmini is used as submodule
set(PUGIXML_INCLUDE_DIR ${PUGIXML_INCLUDE_DIR} CACHE INTERNAL "")
set(EXPR_INCLUDE_DIR ${EXPR_INCLUDE_DIR} CACHE INTERNAL "")
